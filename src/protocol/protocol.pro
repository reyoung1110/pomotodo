#-------------------------------------------------
#
# Project created by QtCreator 2014-11-28T19:49:20
#
#-------------------------------------------------

QT       -= gui

include(../src.pri)

TARGET = protocol
TEMPLATE = lib
CONFIG += staticlib

SOURCES += \
    iprotocol.cpp

HEADERS += \
    iprotocol.h



unix {
    target.path = /usr/lib
    INSTALLS += target
}
