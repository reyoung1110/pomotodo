#ifndef PROTOCOL_H
#define PROTOCOL_H
#include <functional>
#include <memory>
#include <QtCore>


class ITodoItem {
public:
    virtual ~ITodoItem();
    virtual QString getDescription()const = 0;
    virtual int getEstimatedPomosCount()const = 0;
    virtual int getPomoCostedCount()const = 0;
    virtual bool isCompleted()const = 0;
};


class IProtocol
{
public:
    IProtocol();
    virtual ~IProtocol();
    virtual void login(const QString& username, const QString& password,
                       std::function<void(bool,const QString&)> callback) = 0;
    virtual void getTodoItems(std::function< void(bool, const QList<std::shared_ptr<ITodoItem>>&) > callback)= 0;
    virtual bool setAsync(bool isAsync = true) = 0;
    virtual bool getAsync()const = 0;

};
Q_DECLARE_INTERFACE(IProtocol, "me.reyoung.pomotodo.iprotocol")

#endif // PROTOCOL_H
