#ifndef TODOITEM_H
#define TODOITEM_H
#include "pomotodoprotocol.h"

class TodoItem : public ITodoItem
{
private:
    TodoItem(PomotodoProtocol* protocol , const QVariantMap& data);
    friend class PomotodoProtocol;
public:
    virtual QString getDescription()const;
    virtual int getEstimatedPomosCount()const;
    virtual int getPomoCostedCount()const;
    virtual bool isCompleted()const;
private:
    PomotodoProtocol* m_protocol;
    QString m_description;
    qlonglong    m_id;
    int    m_estimatedPomosCount;
    int    m_pomoCostedCount;
    bool   m_isCompleted;
};

#endif // TODOITEM_H
