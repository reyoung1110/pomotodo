#include "pomotodoprotocol.h"
#include "todoitem.h"
#include <QUrl>

static QString base_url="https://pomotodo.com";

PomotodoProtocol::PomotodoProtocol(QObject *parent) :
    QObject(parent), m_accessMgr(new QNetworkAccessManager(this)), m_isAsync(true)
{
#if defined(FIDDLE) && defined(QT_DEBUG)
    this->m_accessMgr->setProxy(QNetworkProxy(QNetworkProxy::HttpProxy,"127.0.0.1",8888));
    this->m_accessMgr->connect(this->m_accessMgr,&QNetworkAccessManager::sslErrors,[](QNetworkReply* reply, QList<QSslError>){
        reply->ignoreSslErrors();
    });
#endif
}

void PomotodoProtocol::login(const QString &username, const QString &password,
                             std::function<void(bool, const QString &)> callback)
{
    doPost("/actions/account/login",{{"username", username},{"password", password},
                                     {"lang","zh-CN"},{"device", "web"},
                                     {"offset", -480}, {"timezone", "Asia/Shanghai"}},
                                   [callback, this](QNetworkReply* reply){
        QByteArray planText = reply->readAll();
        QMap<QString,QVariant> var = QJsonDocument::fromJson(planText).toVariant().toMap();
        bool isError = var.value("error", true).toBool();
        if(!isError){
            m_cookies.clear();
            QNetworkCookie lang;
            lang.setName("i18next");
            lang.setValue("zh-CN");
            m_cookies<<lang;
            QNetworkCookie session;
            session.setName("session");
            QString jsonStr(planText);
            jsonStr.remove(0, jsonStr.indexOf("{\"id\""));
            jsonStr.chop(1);
            session.setValue(jsonStr.toLocal8Bit().toPercentEncoding());
            m_cookies<<session;
            var = var["data"].toMap();
            m_token = var["token"].toString();

            callback(true, QString());
        } else {
            callback(false, var.value("msg","WTF Message").toString());
        }
    });
}

void PomotodoProtocol::getTodoItems(std::function<void (bool, const QList<std::shared_ptr<ITodoItem>>&)> callback)
{
    doGet("/api/todo", {}, [callback, this](bool ok, const QVariantMap& var) {
        ok = ok & !var["error"].toBool();
        if(ok) {
            QVariantList d = var["data"].toList();
            QList<std::shared_ptr<ITodoItem>>  retv;
            for(QVariant& item : d){
                retv << std::shared_ptr<TodoItem>(new TodoItem(this, item.toMap()));
            }
            callback(true, retv);
        } else {
            callback(false, QList<std::shared_ptr<ITodoItem>>());
        }
    });
}

bool PomotodoProtocol::setAsync(bool isAsync)
{
    this->m_isAsync = isAsync;
    return this->m_isAsync;
}

bool PomotodoProtocol::getAsync() const
{
    return this->m_isAsync;
}

void PomotodoProtocol::defaultRequestModifier(QNetworkRequest *req)
{
    Q_UNUSED(req);
}

void PomotodoProtocol::doHttp(QNetworkAccessManager::Operation option, const QString &uri,
                              const QMap<QString, QVariant> &data,
                              std::function<void (QNetworkReply *)> callback,
                              std::function<void(QNetworkRequest *)> requestModifier)
{
    QUrl url(base_url+uri);
    QUrlQuery params;
    for(auto& k: data.keys()){
        params.addQueryItem(k, data[k].toString());
    }
    QNetworkReply * reply =  nullptr;
    const std::function<void(QNetworkRequest*)> reqMod = [this](QNetworkRequest * pReq) {
        QNetworkRequest& req = * pReq;
        if(!this->m_cookies.isEmpty()){
            QVariant var;
            var.setValue(this->m_cookies);
            req.setHeader(QNetworkRequest::CookieHeader,var);
        }
        if(!this->m_token.isEmpty()) {
            req.setRawHeader("X-Lego-Token", this->m_token.toLocal8Bit());
        }
    };


    switch(option) {
    case QNetworkAccessManager::HeadOperation:
    case QNetworkAccessManager::DeleteOperation:
    case QNetworkAccessManager::GetOperation:
    {
        url.setQuery(params);
        QNetworkRequest req(url);
        reqMod(&req);
        requestModifier(&req);
        if(option==QNetworkAccessManager::HeadOperation){
            reply = this->m_accessMgr->head(req);
        } else if(option == QNetworkAccessManager::DeleteOperation){
            reply = this->m_accessMgr->deleteResource(req);
        } else {
            reply = this->m_accessMgr->get(req);
        }
        break;
    }
    case QNetworkAccessManager::PutOperation:
    case QNetworkAccessManager::PostOperation:
    {
        QNetworkRequest req(url);
        reqMod(&req);
        req.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");
        requestModifier(&req);
        if(option == QNetworkAccessManager::PutOperation){
            reply = this->m_accessMgr->put(req, params.toString().toLocal8Bit());
        } else {
            reply = this->m_accessMgr->post(req,params.toString().toLocal8Bit());
        }
        break;
    }
    default:
        break;
    }


    reply->connect(reply,&QNetworkReply::finished, [reply, callback](){
        callback(reply);
    });
    waitIfSync(reply);
}



void PomotodoProtocol::waitIfSync(QNetworkReply *reply)
{
    if(!this->m_isAsync){
        QEventLoop loop;
        connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
        loop.exec();
    }
}

#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2(pomotodo, PomotodoProtocol)
#endif // QT_VERSION < 0x050000
