#-------------------------------------------------
#
# Project created by QtCreator 2014-11-28T20:10:55
#
#-------------------------------------------------

QT       += core gui network
include(../src.pri)

LIBS += -lprotocol
TARGET = pomotodo
TEMPLATE = lib
CONFIG += plugin

DEFINES += FIDDLE

#DESTDIR = $$[QT_INSTALL_PLUGINS]/generic

SOURCES += pomotodoprotocol.cpp \
    todoitem.cpp

HEADERS += pomotodoprotocol.h \
    todoitem.h
OTHER_FILES += pomotodo.json

unix {
    target.path = /usr/lib
    INSTALLS += target
}
