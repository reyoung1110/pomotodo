#ifndef POMOTODOPROTOCOL_H
#define POMOTODOPROTOCOL_H

#include <QGenericPlugin>
#include <protocol/iprotocol.h>
#include <QtNetwork>

class PomotodoProtocol : public QObject, public IProtocol
{
    Q_OBJECT
    Q_INTERFACES(IProtocol)

#if QT_VERSION >= 0x050000
    Q_PLUGIN_METADATA(IID "me.reyoung.pomotodo.IProtocol" FILE "pomotodo.json")
#endif // QT_VERSION >= 0x050000


public:
    PomotodoProtocol(QObject *parent = 0);
    virtual void login(const QString& username, const QString& password,
                       std::function<void(bool, const QString&)> callback);
    virtual void getTodoItems(std::function<void(bool, const QList<std::shared_ptr<ITodoItem>>&) > callback);
    virtual bool setAsync(bool isAsync = true);
    virtual bool getAsync()const;

private:
    inline void doPost(const QString& uri, const QMap<QString, QVariant> &data,
                std::function<void(QNetworkReply*)> callback,
                std::function<void(QNetworkRequest* )> requestModifier = defaultRequestModifier) {
        doHttp(QNetworkAccessManager::PostOperation, uri, data, callback, requestModifier);
    }

    class JsonConvertHelper {
    public:
        JsonConvertHelper(std::function<void (bool, const QMap<QString, QVariant> &)> cb):m_cb(cb){}

        void operator() (QNetworkReply* reply){
            QByteArray planText = reply->readAll();
            QJsonDocument doc = QJsonDocument::fromJson(planText);
            if(!doc.isNull()){
                m_cb(true,doc.toVariant().toMap());
            } else {
                m_cb(false,QMap<QString,QVariant>());
            }
        }

    private:
        std::function<void (bool, const QMap<QString, QVariant> &)> m_cb;
    };



    static void defaultRequestModifier(QNetworkRequest* req);

    inline void doPost(const QString &uri, const QMap<QString, QVariant> &data,
                       std::function<void (bool, const QMap<QString, QVariant> &)> callback,
                       std::function<void(QNetworkRequest* )> requestModifier = defaultRequestModifier){
        doPost(uri, data,JsonConvertHelper(callback),requestModifier);
    }

    inline void doGet(const QString &uri, const QMap<QString, QVariant> &data,
                      std::function<void (bool, const QMap<QString, QVariant> &)> callback,
                      std::function<void(QNetworkRequest* )> requestModifier = defaultRequestModifier){
        doGet(uri,data, JsonConvertHelper(callback), requestModifier);
    }

    inline void doGet(const QString& uri, const QMap<QString, QVariant> & data,
                      std::function<void(QNetworkReply*)> callback, std::function<void(QNetworkRequest* )> requestModifier = defaultRequestModifier){
        doHttp(QNetworkAccessManager::GetOperation, uri, data, callback, requestModifier);
    }

    void doHttp(QNetworkAccessManager::Operation option, const QString& uri,
                const QMap<QString, QVariant> &data,
                std::function<void(QNetworkReply*)> callback,
                std::function<void(QNetworkRequest* )> requestModifier = defaultRequestModifier);

    void waitIfSync(QNetworkReply* reply);


    QNetworkAccessManager* m_accessMgr;
    bool m_isAsync;
    QList<QNetworkCookie>  m_cookies;
    QString m_token;
};

#endif // POMOTODOPROTOCOL_H
