#include "todoitem.h"

TodoItem::TodoItem(PomotodoProtocol *protocol, const QVariantMap &data)
    :m_protocol(protocol)
{
    this->m_description = data["description"].toString();
    this->m_id = data["id"].toLongLong();
    this->m_estimatedPomosCount = data["estimated_pomos"].toInt();
    this->m_pomoCostedCount = data["pomo_costed"].toInt();
    this->m_isCompleted = data["completed"].toBool();
    qDebug()<<this->m_id <<","<<this->m_estimatedPomosCount<<","<<this->m_pomoCostedCount<<","<<this->m_isCompleted;
}

QString TodoItem::getDescription() const
{
    return this->m_description;
}

int TodoItem::getEstimatedPomosCount() const
{
    return this->m_estimatedPomosCount;
}

int TodoItem::getPomoCostedCount() const
{
    return this->m_pomoCostedCount;
}

bool TodoItem::isCompleted() const
{
    return this->m_isCompleted;
}
