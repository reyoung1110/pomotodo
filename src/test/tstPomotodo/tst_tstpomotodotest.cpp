#include <QString>
#include <QtTest>
#include <QCoreApplication>
#include <QDebug>
#include <protocol/iprotocol.h>
#include <memory>




class TstPomotodoTest : public QObject
{
    Q_OBJECT

public:
    TstPomotodoTest();

private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();
    void testLogin();
    void testGetTodo();
private:
    std::shared_ptr<IProtocol> mProtocol;
    std::shared_ptr<QPluginLoader> mLoader;
};

TstPomotodoTest::TstPomotodoTest()
{
}

void TstPomotodoTest::initTestCase()
{
    mLoader = std::make_shared<QPluginLoader>("pomotodo.dll");
    Q_ASSERT(mLoader->load());
    QObject* obj = mLoader->instance();
    this->mProtocol = std::shared_ptr<IProtocol>(dynamic_cast<IProtocol*>(obj));
    Q_ASSERT(this->mProtocol.get()!=NULL);
    this->mProtocol->setAsync(false);
    Q_ASSERT(!this->mProtocol->getAsync());
}

void TstPomotodoTest::cleanupTestCase()
{
}

void TstPomotodoTest::testLogin()
{
    bool logined =false;
    this->mProtocol->login(_TSTUNAME, _TSTPASSWORD, [&logined](bool ok, const QString& errCode){
        logined = ok;
    });
    Q_ASSERT(logined);
}

void TstPomotodoTest::testGetTodo()
{
    bool getTodo = false;
    this->mProtocol->getTodoItems([&getTodo](bool ok, const QList<std::shared_ptr<ITodoItem>> & lst){
        getTodo = ok;
    });
    Q_ASSERT(getTodo);
}

QTEST_MAIN(TstPomotodoTest)

#include "tst_tstpomotodotest.moc"
