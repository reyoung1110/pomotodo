#-------------------------------------------------
#
# Project created by QtCreator 2014-11-28T23:24:45
#
#-------------------------------------------------

QT       += testlib
include(../test.pri)
QT       -= gui

TARGET = tst_tstpomotodotest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += tst_tstpomotodotest.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"
